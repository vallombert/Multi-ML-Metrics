(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

(** Rates will be between 0 and max_rate *)
let max_rate = 10.0

(**
 * Returns number between 0 and 10,
 * 0  if value <= min
 * 10 if value >= max
 * something between when value is somewhere between
 * *)
let rate_lineal min max value =
    let min = float_of_int min
    and max = float_of_int max in
    let rate = (value -. min) /. (max -. min) *. max_rate in
    if rate > max_rate then
        max_rate
    else if rate < 0.0 then
        0.0
    else
        rate

(** Rate function metric
 * @param name of the mteric
 * @param value of the metric
 * *)
let get_function_metric_rate name value =
    match name with
    | "CC"  -> rate_lineal 30 8 value
    | "LOC" -> rate_lineal 100 20 value
    | "HD"  -> rate_lineal 80 25 value 
    | "HV"  -> rate_lineal 6000 1000 value
    | "HE"  -> rate_lineal 100000 1000 value
    | "MI"  -> rate_lineal 65 95 value
    |  _    -> failwith ("Cannot rate function metric " ^ name)

(** Rate module metric
 * @param name of the mteric
 * @param value of the metric
 * *)
let get_module_metric_rate name value =
    match name with
    | "N"   -> rate_lineal 50 25 value
    | "LOC" -> rate_lineal 1000 300 value
    | "HD"  -> rate_lineal 200 100 value
    | "HV"  -> rate_lineal 50000 15000 value
    | "HE"  -> rate_lineal 10000000 1000000 value
    | "MI"  -> rate_lineal 65 95 value
    | "avgCC"  -> rate_lineal 15 6 value
    | "avgLOC" -> rate_lineal 80 15 value
    | "avgHV"  -> rate_lineal 10000 1000 value
    | _ -> failwith ("Cannot rate module metric " ^ name)
