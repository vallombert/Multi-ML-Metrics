(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

type halstead_data

(** Returns data used to calculate metrics *)
val get_data: Tokens.code -> halstead_data

(** Given data, returns HD *)
val get_difficulty: halstead_data -> float

(** Given data, returns HV *)
val get_volume: halstead_data -> float

(** Given data, returns HE *)
val get_effort: halstead_data -> float

(** Given code, returns HE *)
val get_code_effort: Tokens.code -> float
