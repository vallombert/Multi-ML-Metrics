## Description

Multi-ML-Metrics is a simple OCaml code analysis tool. It can compute some metrics of functions and modules: cyclomatic complexity, the number of lines of code, Halstead complexity measures (difficulty, volume and effort) and Maintainability Index. Multi-ML-Metrics generates reports in three formats: HTML, simple XML or plain text. 

*Multi-ML-Metrics is a OCaml Metrics fork (http://students.mimuw.edu.pl/~ms248283/ocaml-metrics/)*
