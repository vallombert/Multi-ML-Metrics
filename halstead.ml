(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

module TokenSet = Set.Make(Tokens)

type halstead_data = {
    distinct_operators : float;
    distinct_operands : float;
    total_operators : float;
    total_operands : float;
}

let is_operator = function
    | Tokens.ID _ | Tokens.LITERAL _ | Tokens.VECTOR _ | Tokens.REPLICATE _ 
    | Tokens.EOL _ | Tokens.EOF                                            -> false
    | _ -> true

let is_operand = function
    | Tokens.ID _ | Tokens.LITERAL _ | Tokens.VECTOR _ | Tokens.REPLICATE _ -> true
    | _ -> false

(* Returns data used to calculate metrics *)
let get_data code =
    let update (n1, n2, set1, set2) token =
        if is_operator token then
            (n1 + 1, n2, TokenSet.add token set1, set2)
        else if is_operand token then
            (n1, n2 + 1, set1, TokenSet.add token set2)
        else
            (n1, n2, set1, set2)
    in
    let (n1, n2, set1, set2) =
        List.fold_left update (0, 0, TokenSet.empty, TokenSet.empty) code
    in
    {
        distinct_operators = float_of_int (TokenSet.cardinal set1);
        distinct_operands  = float_of_int (TokenSet.cardinal set2);
        total_operators    = float_of_int n1;
        total_operands     = float_of_int n2;
    }

(* Given data, returns HD *)
let get_difficulty data =
    (data.distinct_operators /. 2.0) *. (data.total_operands /. data.distinct_operands)

(* Given data, returns HV *)
let get_volume data =
    let length = data.total_operators +. data.total_operands
    and vocabulary = data.distinct_operands +. data.distinct_operators in
    length *. log vocabulary /. log 2.0

(* Given data, returns HE *)
let get_effort data =
    get_difficulty data *. get_volume data

(* Given code, returns HE *)
let get_code_effort code =
    get_effort (get_data code)
