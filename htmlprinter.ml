(****************************************************************************)
(*                                                                          *)
(*   Multi-ML Metrics                                                       *)
(*   2017 by Frédéric Gava                                                  *)
(*   from the following work: OCaml Metrics                                 *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

open Metrics

(* Metrics to show when describing module, in the right order *)
let module_show =
    [ "N"; "LOC"; "HD"; "HV"; "HE"; "avgCC"; "avgLOC"; "avgHV"; "MI" ]

(* Metrics to show when describing function/method, in the right order *)
let function_show =
    [ "LOC"; "CC"; "HD"; "HV"; "HE"; "MI" ]

(* Given a rate <0; Rate.max_rate>, generates hex color of a cell in the table *)
let color_of_rate rate =
    let green = int_of_float (rate /. Rate.max_rate *. 255.0) in
    Printf.sprintf "#ff%02x00" green

let map_concat f l =
    String.concat "" (List.map f l)

(* First part of the output *)
let html_header =
    "<html>\n" ^
    "  <head>\n" ^ 
    "    <title>BSML/Multi-ML metrics report</title>\n" ^
    "    <style type=\"text/css\">" ^
    "    h1 { background-color: #AAAAAA; margin-top:50px; } " ^
    "    table { border: 2px #000000 solid; border-collapse: collapse; } " ^
    "    td { width: 50px; border: 1px solid #aaaaaa; } " ^
    "    td.wide {border-right: 2px solid black; width: auto; padding: 0px 5px; } " ^
    "    </style>\n" ^
    "</head>\n" ^
    "  <body>\n" ^
    "    <h1>Calculated metrics</h1>\n" ^
    "    <ul>\n" ^
    map_concat (fun (short, long, descr) -> Printf.sprintf
        "    <li><b>%s</b> (<i>%s</i>) &ndash; %s</li>\n" short long descr
    ) [
    "LOC", "Lines Of Code",
    "number of lines of the function source or the module source";

    "CC", "McCabe Cyclomatic Complexity",
    "the number of linearly independent paths through the source, ";

    "HD", "Halstead Difficulty",
    "how complex the code is (eg. how many different operators are used)";

    "HV", "Halstead Volume",
    "how long the code is";

    "HE", "Halstead Effort",
    "HD &times; HV, how many brain operations are needed to create the code :)";

    "N", "Number of functions",
    "Number of functions and methods in a given module";

    "avgCC", "average Cyclomatic Complexity",
    "average CC of the functions and methods in a given module";

    "avgLOC", "average Lines of Code",
    "average LOC of the functions and methods in a given module";

    "avgHV", "average Halstead Volume",
    "average HV of the functions and methods in a given module";
    
    "MI", "Maintainability Index",
    "Depends on CC, LOC and HV (or avgCC, avgLOC, avgHV), how easy to maintain the code is"
    ] ^
    "    </ul>\n" ^
    "    <p>Learn more about <a " ^
    "href=\"http://en.wikipedia.org/wiki/Cyclomatic_complexity\">Cyclomatic_" ^
    "Complexity<a/> and <a href=\"http://en.wikipedia.org/wiki/" ^
    "Halstead_complexity_measures\">Halstead complexity measures</a>.</p>"


(* Generate table with module metrics *)
let module_metrics_to_string metrics =
    map_concat (fun name -> Printf.sprintf
        "      <tr bgcolor=\"%s\"><td class=\"wide\"><b>%s</b></td> <td>%.0f</td></tr>\n"
            (color_of_rate (Rate.get_module_metric_rate name (get_metric metrics name)))
            name
            (get_metric metrics name)
    ) module_show

(* Generate table row describing one function *)
let function_to_row (typename, name, line, metrics) =
    Printf.sprintf
        "      <tr><td class=\"wide\">%s <tt><b>%s</b></tt></td><td class=\"wide\">%d</td>"
        typename name line ^
    map_concat (fun mn -> Printf.sprintf "<td bgcolor=\"%s\">%.0f</td>"
       (color_of_rate (Rate.get_function_metric_rate mn (get_metric metrics mn)))
       (get_metric metrics mn)
    ) function_show ^
    "</tr>\n"

(* Generate table describing all functions in one module *)
let functions_to_string functions =
    let f = List.sort (fun (_,n1,_,_) (_,n2,_,_) -> compare n1 n2) functions in
    "    <table>\n" ^
    "      <tr><td class=\"wide\"><b>name</b></td><td class=\"wide\"><b>line</b></td>" ^
    map_concat (Printf.sprintf "<td><b>%s</b></td>") function_show ^
    "</tr>\n" ^
    map_concat function_to_row f ^
    "    </table>\n"

(* Describe details of one module *)
let module_to_string (name, metrics, functions) =
    "    <h1>Module <tt>" ^ name ^ "</tt></h1>\n\n" ^
    "    <h2>General metrics</h2>\n" ^
    "    <table>\n" ^
    module_metrics_to_string metrics ^
    "    </table>\n\n" ^
    "    <h2>Functions and methods</h2>\n" ^
    functions_to_string functions ^
    "\n"

(* One row in the table 'top 10 modules' *)
let module_to_summary_row (name, metrics, _) =
    "      <tr><td class=\"wide\"><b><tt>" ^ name ^ "</b></tt></td>" ^
    map_concat (fun mn -> Printf.sprintf "<td bgcolor=\"%s\">%.0f</td>"
       (color_of_rate (Rate.get_module_metric_rate mn (get_metric metrics mn)))
       (get_metric metrics mn)
    ) module_show ^
    "</tr>\n"

(* One row in the table 'top 10 modules' *)
let function_to_summary_row (modulename, (typename, name, line, metrics)) =
    Printf.sprintf
        "      <tr><td class=\"wide\"><b><tt>%s</tt></b></td><td class=\"wide\">%s <b><tt>%s</b></tt></td><td class=\"wide\">%d</td>"
        modulename typename name line ^
    map_concat (fun mn -> Printf.sprintf "<td bgcolor=\"%s\">%.0f</td>"
       (color_of_rate (Rate.get_function_metric_rate mn (get_metric metrics mn)))
       (get_metric metrics mn)
    ) function_show ^
    "</tr>\n"

(* Generate the table 'top 10 modules' *)
let top_worst_modules modules  count =
    let worst = Array.of_list modules in
    Array.sort (fun (_, metr1, _) (_, metr2, _) ->
        compare (Metrics.get_metric metr1 "MI") (Metrics.get_metric metr2 "MI")
    ) worst;
    let n = if Array.length worst >= count then count else Array.length worst in
    "    <h1>Top " ^ string_of_int count ^ " worst modules</h1>\n" ^
    "    <table>\n" ^
    "    <tr><td class=\"wide\"><b>name</b></td>" ^
    map_concat (Printf.sprintf "<td><b>%s</b></td>") module_show ^
    "</tr>\n" ^
    map_concat module_to_summary_row (Array.to_list (Array.sub worst 0 n)) ^
    "    </table>\n"

(* Generate the table 'top N functions' *)
let top_worst_functions modules count =
    let worst =
        Array.of_list (List.fold_left (fun acc (n,_,f) ->
            let functions =
                List.map (fun e -> (n, e)) f
            in 
            functions @ acc
        ) [] modules)
    in
    Array.sort (fun (_,(_, _, _, metr1)) (_,(_, _, _, metr2)) ->
        compare (Metrics.get_metric metr1 "MI") (Metrics.get_metric metr2 "MI")
    ) worst;
    let n = if Array.length worst >= count then count else Array.length worst in
    "    <h1>Top " ^ string_of_int count ^" worst functions</h1>\n" ^
    "    <table>\n" ^
    "    <tr><td class=\"wide\"><b>module name</b></td> " ^
    "<td class=\"wide\"><b>name</b></td> <td class=\"wide\"><b>line</b></td>" ^
    map_concat (Printf.sprintf "<td><b>%s</b></td>") function_show ^
    "</tr>\n" ^
    map_concat function_to_summary_row (Array.to_list (Array.sub worst 0 n)) ^
    "    </table>\n"

(* The end *)
let html_footer =
    "  <hr/><p>Generated by MultiML Metrics by Fr&eacute;d&eacute;ric Gava (from the code of <a href=\"http://forge.ocamlcore.org/projects/ocaml-metrics/\"> OCaml Metrics </a>).<br/>\n" ^
    "  <a href=\"https://www.lacl.fr/vallombert/Multi-ML/\">" ^
    "  MultiML web page </a></p>\n" ^
    "  </body>\n" ^ 
    "</html>"

(* Choose only bad functions from function_metrics list *)
let filter_functions max_mi min_loc min_cc =
    List.filter (fun (_, _, _, metrics) ->
        Metrics.get_metric metrics "MI" <= float_of_int max_mi &&
        Metrics.get_metric metrics "LOC" >= float_of_int min_loc &&
        Metrics.get_metric metrics "CC" >= float_of_int min_cc)

let generate_output modules n_functions n_modules max_mi min_loc min_cc =
    let modules = List.sort (fun (n1, _, _) (n2, _, _) -> compare n1 n2) modules in
    html_header ^
    top_worst_modules modules n_modules ^
    top_worst_functions modules n_functions ^ 
    let modules = List.map (fun (name, metrics, funs) ->
        (name, metrics, filter_functions max_mi min_loc min_cc funs)) modules in
    let modules = List.filter (fun (_, _, l) -> l <> []) modules in
    map_concat module_to_string modules ^
    html_footer
