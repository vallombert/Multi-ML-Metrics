(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

let map_concat f l =
    String.concat "" (List.map f l)

let metrics_to_string metrics rate_fun indent =
    map_concat (fun (name, value) -> Printf.sprintf
        "%s<metric name=\"%s\" value=\"%.1f\" rate=\"%.1f\" />\n"
        indent name value (rate_fun name value)
    ) (Metrics.metrics_to_list metrics)

let function_to_string (typename, name, line, metrics) =
    Printf.sprintf "    <%s name=\"%s\" line=\"%d\">\n" typename name line ^
    metrics_to_string metrics Rate.get_function_metric_rate "      " ^
    Printf.sprintf "    </%s>\n" typename

let module_to_string (name, metrics, functions) =
    Printf.sprintf "  <module name=\"%s\">\n" name ^
    metrics_to_string metrics Rate.get_module_metric_rate "    " ^
    map_concat function_to_string functions ^
    "  </module>\n"

let generate_output modules _ _ _ _ _ =
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" ^
    "<ocamlmetrics>\n" ^
    map_concat module_to_string modules ^
    "</ocamlmetrics>\n"
