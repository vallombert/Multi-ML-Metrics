(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

(* List of files to check *)
let modules = ref []

(* Function which will generate output *)
let generator = ref Htmlprinter.generate_output

(* Command line parameters *)

(* Number of worst functions to show *)
let n_worst_modules = ref 10

(* Number of worst mopdules to show *)
let n_worst_functions = ref 10

(* Maximum MI of functions shown in the report *)
let max_mi = ref 1000

(* Minimum LOC of functions shown in the report *)
let min_loc = ref 1

(* Minimum CC of functions shown in the report *)
let min_cc = ref 0

(* Some setters *)
let set_ascii () = generator := Asciiprinter.generate_output
let set_html () = generator := Htmlprinter.generate_output
let set_xml () = generator := Xmlprinter.generate_output

(* Usage message *)
let usage_msg =
    Printf.sprintf "Usage: %s [options] files..." Sys.argv.(0)

(* Speclist for Arg.parse *)
let speclist = Arg.align [
    "-a", Arg.Unit set_ascii, " Generate output in palin ASCII format";
    "-h", Arg.Unit set_html, " Generate output in HTML format (default)";
    "-x", Arg.Unit set_xml, " Generate output in XML format";
    "-worst-modules", Arg.Set_int n_worst_modules, "<number> Show <number> worst modules in the summary (HTML only)";
    "-worst-functions", Arg.Set_int n_worst_functions, "<number> Show <number> worst functions in the summary (HTML only)";
    "-max-mi", Arg.Set_int max_mi, "<number> Show only functions with MI less or equal to <number> (HTML only)";
    "-min-loc", Arg.Set_int min_loc, "<number> Show only functions with LOC greater or equal to <number> (HTML only)";
    "-min-cc", Arg.Set_int min_cc, "<number> Show only functions with CC greater or equal to <number> (HTML only)";
]

(* The main function *)
let main () =
    Arg.parse speclist (fun s -> modules := s :: !modules) usage_msg;
    if !modules = [] then
        Arg.usage speclist usage_msg
    else
        let metrics = List.map Metrics.get_module_metrics !modules in
        print_endline (!generator metrics
            !n_worst_functions
            !n_worst_modules
            !max_mi !min_loc !min_cc)

(* Let's start *)
let _ = main ()
