(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

let value_of = function
    | Tokens.IF
    | Tokens.WHILE
    | Tokens.FOR
    | Tokens.MATCH
    | Tokens.TRY
    | Tokens.FUN
    | Tokens.FUNCTION
    | Tokens.REC 
    | Tokens.MULTI -> 1.0
    | _ -> 0.0


(** Returns McCabe's Cyclomatic Complexity of the code *)
let get_cc tokens =
    List.fold_left (fun acc token -> acc +. (value_of token)) 1.0 tokens
