(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

(**
 * Calculates Code Maintainability Index
 * @param cc Cyclomatic Complexity
 * @param loc Lines of code
 * @param hv Halstead's Volume
 * *)
let get_mi cc loc hv =
    let log_hv = if hv >= 1.0 then log hv else 0.0
    and log_loc = if loc >= 1.0 then log loc else 0.0 in
    171.0 -. (5.2 *. log_hv) -. (0.23 *. cc) -. (16.2 *. log_loc)
