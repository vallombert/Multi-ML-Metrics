(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

let ascii_of_rate rate =
    let max = Rate.max_rate in
    if rate < max /. 4.0 then "!!!"
    else if rate < max /. 2.0 then "!!"
    else if rate < 3.0 *. max /. 4.0 then "!"
    else ""

let map_concat f l =
    String.concat "" (List.map f l)

let metrics_to_string metrics rate_fun =
    map_concat (fun (name, value) -> Printf.sprintf
        "%6s %12.1f %s\n" name value (ascii_of_rate (rate_fun name value))
    ) (Metrics.metrics_to_list metrics)

let function_to_string (typename, name, line, metrics) =
    Printf.sprintf "%s %s: %d\n" typename name line ^
    metrics_to_string metrics Rate.get_function_metric_rate

let module_to_string (name, metrics, functions) =
    let functions =
        List.sort (fun (_, n1, _, _) (_, n2, _, _) -> compare n1 n2) functions
    in
    "module " ^ name ^ "\n" ^
    metrics_to_string metrics Rate.get_module_metric_rate ^
    map_concat function_to_string functions

let generate_output modules _ _ _ _ _ =
    let modules = 
        List.sort (fun (n1, _, _) (n2, _, _) -> compare n1 n2) modules
    in
    map_concat module_to_string modules
