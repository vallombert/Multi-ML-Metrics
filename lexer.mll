{

(****************************************************************************)
(*                                                                          *)
(*   Multi-ML Metrics                                                       *)
(*   2017 by Frédéric Gava                                                  *)
(*   from the following work: OCaml Metrics                                 *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)
    open Tokens
    open Lexing

    let keywords = Hashtbl.create 200
    let _ = List.iter (fun (kwd, tok) -> Hashtbl.add keywords kwd tok) [
        "and", AND;
        "as", AS;
        "assert", ASSERT;
        "begin", BEGIN;
        "class", CLASS;
        "constraint", CONSTRAINT;
        "do", DO;
        "done", DONE;
        "downto", DOWNTO;
        "else", ELSE;
        "end", END;
        "exception", EXCEPTION;
        "external", EXTERNAL;
        "false", FALSE;
        "for", FOR;
        "fun", FUN;
        "function", FUNCTION;
        "functor", FUNCTOR;
        "if", IF;
        "in", IN;
        "include", INCLUDE;
        "inherit", INHERIT;
        "initializer", INITIALIZER;
        "lazy", LAZY;
        "let", LET;
        "match", MATCH;
        "method", METHOD;
        "module", MODULE;
        "mutable", MUTABLE;
        "new", NEW;
        "object", OBJECT;
        "of", OF;
        "open", OPEN;
        "or", OR;
        "private", PRIVATE;
        "rec", REC;
        "sig", SIG;
        "struct", STRUCT;
        "then", THEN;
        "to", TO;
        "true", TRUE;
        "try", TRY;
        "type", TYPE;
        "val", VAL;
        "value", VALUE;
        "virtual", VIRTUAL;
        "when", WHEN;
        "while", WHILE;
        "with", WITH;
        "mod", MOD;
        "land", LAND;
        "lor", LOR;
        "lxor", LXOR;
        "lsl", LSL;
        "lsr", LSR;
        "asr", ASR;
        (* BSML *)
        "proj", PROJ;
        "put",  PUT;
        "mkpar", MKPAR;
        "this", THIS;
        "bps_p", BSPP;
        (* MULTI *)
        "where", WHERE;
         "node", NODE;
         "leaf", LEAF;
         "multi", MULTI;
         "finally", FINALLY;
         "tree", TREE;
         "gid", GID;
         "mktree", GID;

    ]

    let update_eol lexbuf =
        lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with
            pos_lnum = lexbuf.lex_curr_p.pos_lnum + 1
        }
}

let space = [' ' '\t']
let digit = ['0'-'9']
let lowercase = ['a'-'z' '\223'-'\246' '\248'-'\255' '_']
let uppercase = ['A'-'Z' '\192'-'\214' '\216'-'\222']
let identchar =
  ['A'-'Z' 'a'-'z' '_' '\192'-'\214' '\216'-'\246' '\248'-'\255' '\'' '0'-'9']
let noidentchar =
  [^ 'A'-'Z' 'a'-'z' '_' '\192'-'\214' '\216'-'\246' '\248'-'\255' '\'' '0'-'9' ]
let symbolchar =
  ['!'  '%' '&' '*' '+' '-' '.' '/' ':' '<' '=' '>' '?' '@' '^' '|' '~']
let othersymbolchar =
    [ '\'' ',' '[' ']' '(' ')' '{' '}' ';' '\\' '`']
let eol =
    ('\010' | '\013' | "\013\010")

rule token = parse
    | space+   { token lexbuf }
    | lowercase identchar* as lxm { try Hashtbl.find keywords lxm with Not_found -> ID lxm }
    | uppercase identchar* as lxm { ID lxm }
    | '#' (identchar* as lxm) '#' { try Hashtbl.find keywords lxm with Not_found -> REPLICATE lxm }
    | '$' (identchar* as lxm) '$'  { try Hashtbl.find keywords lxm with Not_found -> VECTOR lxm }
    | digit+ identchar* as lxm    { LITERAL lxm }
    | digit+ '.' digit* identchar* as lxm { LITERAL lxm }
    | "()" as lxm                 { ID lxm }
    | "<<"                        {BeginVector}
    | ">>"                        {EndVector}    
    | symbolchar+ as lxm          { SYMBOL lxm }
    | othersymbolchar as lxm      { SYMBOL (String.make 1 lxm) }
    | "'" _ "'" as lxm            { LITERAL lxm }
    | "'\\\"'" as lxm             { LITERAL lxm }
    | '"'      { strings lexbuf }
    | "(*"     { comments 0 lexbuf }
    | eol      { let n = lexbuf.lex_curr_p.pos_lnum in update_eol lexbuf; EOL n }
    | eof      { EOF }
and comments level = parse
    | "*)" { if level = 0 then token lexbuf else comments (level - 1) lexbuf }
    | "(*" { comments (level + 1) lexbuf }
    | eol  { update_eol lexbuf; comments level lexbuf }
    | eof  { EOF }
    | _    { comments level lexbuf }
and strings = parse
    | eol    { update_eol lexbuf; strings lexbuf }
    | "\\" _ { strings lexbuf }
    | "\""   { LITERAL "string" }
    | _      { strings lexbuf }
