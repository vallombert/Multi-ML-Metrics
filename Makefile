OCAMLOPT  = ocamlopt
OCAMLC    = ocamlc
OCAMLLEX  = ocamllex
OCAMLYACC = ocamlyacc
OCAMLDEP  = ocamldep
CFLAGS    = -dtypes
LDFLAGS   = unix.cmxa 

EXEC = multiMLmetrics

multiMLmetrics: tokens.cmx lexer.cmx mccabe.cmx loc.cmx halstead.cmx MI.cmx metrics.cmx rate.cmx xmlprinter.cmx htmlprinter.cmx asciiprinter.cmx multiMLmetrics.cmx

all: $(EXEC)

$(EXEC):
	$(OCAMLOPT) $(CFLAGS) -o $@ $(LDFLAGS) $^

.depend:
	$(OCAMLDEP) -native *.ml *.mli > .depend

%.ml: %.mll
	$(OCAMLLEX) $<
%.cmx: %.ml
	$(OCAMLOPT) $(CFLAGS) -c $<
%.cmi: %.mli
	$(OCAMLC) $(CFLAGS) -c $<
%.cmi: %.ml
	$(OCAMLC) $(CFLAGS) -c $<
%.cmo: %.ml
	$(OCAMLC) $(CFLAGS) -c $<

clean:
	rm -f *.cmi *.cmx *.cmo $(EXEC) lexer.ml lexer.mli *.o *.annot .depend

.PHONY: clean

include .depend
