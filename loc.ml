(****************************************************************************)
(*                                                                          *)
(*   Multi-ML Metrics                                                       *)
(*   2017 by Frédéric Gava                                                  *)
(*   from the following work: OCaml Metrics                                 *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

let value_of = function
    | Tokens.EOL _ -> 1.0
    | _ -> 0.0


(** Removes duplicated EOLs *)
let remove_eol_eol code =
    let rec remove_eol_eol_rec result was_eol = function
        | [] -> result
        | Tokens.EOL n :: t ->
                if was_eol then
                    remove_eol_eol_rec result true t
                else
                    remove_eol_eol_rec (Tokens.EOL n :: result) true t
        | h :: t ->
                remove_eol_eol_rec (h :: result) false t
    in remove_eol_eol_rec [] false code


(** Returns the number of lines of given code *)
let get_loc code =
    let filtered = remove_eol_eol code in
    List.fold_left (fun acc token -> acc +. (value_of token)) 0.0 filtered
