(****************************************************************************)
(*                                                                          *)
(*   OCaml Metrics                                                          *)
(*   Copyright (C) 2009 Marcin Sulikowski                                   *)
(*                                                                          *)
(*  This program is free software: you can redistribute it and/or modify    *)
(*  it under the terms of the GNU General Public License as published by    *)
(*  the Free Software Foundation, either version 3 of the License, or       *)
(*  (at your option) any later version.                                     *)
(*                                                                          *)
(*  This program is distributed in the hope that it will be useful,         *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of          *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *)
(*  GNU General Public License for more details.                            *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.   *)
(*                                                                          *)
(****************************************************************************)

open Tokens
open Lexing

module StringMap = Map.Make(String)

type metrics = float StringMap.t
type function_metrics = string * string * int * metrics
type module_metrics = string * metrics * (function_metrics list)


exception Not_a_function


(*
 * Returns contents of a given .ml file as a list of tokens,
 * in reversed oreder (EOF is the first element)
 * *)
let tokens_of_file filename =
    let channel = open_in filename in
    let lexbuf = Lexing.from_channel channel in
    let rec get_tokens result =
        match Lexer.token lexbuf with
        | EOF -> EOF :: result
        | t   -> get_tokens (t :: result)
    in
    let tokens =
        try
            get_tokens []
        with Failure s ->
            let p = lexbuf.lex_start_p in
            failwith (s ^ " " ^ filename ^ " " ^ string_of_int p.pos_lnum)
    in
    close_in channel;
    tokens


(*
 * Cuts the reversed list of tokens into pieces
 * representing single declarations
 * *)
let split_source source =
    let rec cut_rec tokens level result last_declaration =
        assert (level >= 0);
        match tokens with
        | []    -> last_declaration :: result
        | h::t  ->
            begin
                match h with
                | OPEN | TYPE | MODULE | CLASS | EXCEPTION | METHOD
                | INITIALIZER | VAL | EXTERNAL ->
                    if (level = 0) then
                        cut_rec t 0 ((h ::last_declaration) :: result) []
                    else
                        cut_rec t level result (h :: last_declaration)
                | LET ->
                    if level = 0 then
                        cut_rec t 0 ((LET :: last_declaration) :: result) []
                    else
                        cut_rec t (level - 1) result (LET :: last_declaration)
                | AND ->
                    if level = 0 then
                        cut_rec t 0 ((LET :: last_declaration) :: result) []
                    else
                        cut_rec t level result (AND :: last_declaration)
                | IN ->
                    cut_rec t (level + 1) result (IN :: last_declaration)
                | _ ->
                    cut_rec t level result (h :: last_declaration)
            end
    in cut_rec source 0 [] []

(* Returns name of the module given it's source file name *)
let name_of_module filename =
    String.capitalize (Filename.chop_extension (Filename.basename filename))

(* Returns the name of function/method from given code *)
let rec name_of_function code =
    match code with
    | [] -> "???"
    | (ID s) :: t -> s
    | _ :: t -> name_of_function t


(* Returns the type of given code (function or method) *)
let type_of code =
    match code with
    | LET :: _ -> "function"
    | LET :: MULTI :: _ -> "multi function"
    | LET :: MULTI :: TREE :: _ -> "multi function"
    | METHOD :: _ -> "method"
    | _ -> raise Not_a_function

(* Returns line number of the first token in given code *)
let rec line_number_of code =
    match code with
    | [] -> 0
    | EOL n :: _ -> n
    | _ :: t -> line_number_of t

(* Adds a new metric to the set *)
let add_metric metrics (name, value) =
    StringMap.add name value metrics


(* Gets value of a metric *)
let get_metric metrics name =
    StringMap.find name metrics

(* Creates metrics object from list of pairs (name, value) *)
let metrics_of_list l =
    List.fold_left add_metric StringMap.empty l

(* Gets all metrics as the list of pairs (name, value) *)
let metrics_to_list metrics =
    StringMap.fold (fun k v acc -> (k, v) :: acc) metrics []

(* Returns function_metrics object *)
let get_function_metrics code =
    let h_data = Halstead.get_data code in
    let cc = Mccabe.get_cc code
    and loc = Loc.get_loc code
    and hv = Halstead.get_volume h_data
    and hd = Halstead.get_difficulty h_data
    and he = Halstead.get_effort h_data in
    let mi = MI.get_mi cc loc hv in
    (type_of code, name_of_function code, line_number_of code, metrics_of_list [
        "CC", cc;
        "LOC", loc;
        "HV", hv;
        "HD", hd;
        "HE", he;
        "MI", mi;
    ])

(* Given a file name returns module_metrics *)
let get_module_metrics filename =
    let code = tokens_of_file filename in
    let splitted_source = split_source code in
    (* Calculate metrics of all functions and methods *)
    let function_metrics = List.fold_left (fun acc f ->
        try
            (get_function_metrics f) :: acc
        with Not_a_function -> acc
    ) [] splitted_source in
    (* Calculate metrics of the module *)
    let data = Halstead.get_data code in
    let hd = Halstead.get_difficulty data
    and hv = Halstead.get_volume data
    and he = Halstead.get_effort data
    and loc = Loc.get_loc code in
    (* Calculate average metrics of functions/methods *)
    let (sum_cc, sum_loc, sum_hv, sum_he) =
        List.fold_left (fun (scc, sloc, shv, she) (_, _, _, ml) -> 
            (scc +. get_metric ml "CC",
            sloc +. get_metric ml "LOC",
            shv +. get_metric ml "HV",
            she +. get_metric ml "HE")
    ) (0.0, 0.0, 0.0, 0.0) function_metrics in
    let n = float_of_int (List.length function_metrics) in
    let avg_cc = if n > 0.0 then sum_cc /. n else 0.0
    and avg_loc = if n > 0.0 then  sum_loc /. n else 0.0
    and avg_hv = if n > 0.0 then  sum_hv /. n else 0.0 in
    (* Calculate Maintainability index of the module *)
    let mi = MI.get_mi avg_cc avg_loc avg_hv in
    (* Create metrics object *)
    let module_metrics = metrics_of_list [
        "HD", hd;
        "HV", hv;
        "HE", he;
        "LOC", loc;
        "N", n;
        "avgCC", avg_cc;
        "avgLOC", avg_loc;
        "avgHV", avg_hv;
        "MI", mi
    ] in
    (name_of_module filename, module_metrics, function_metrics)
